####Relevant packages
library(dplyr)
library(tidyr)
library(argusDS.FZ.utils)
library(readxl)
library(countrycode)
library(zoo)
library(forecast)
library(argusDS.GTT)
library(rugarch)UOD
library(xts)
library(tempdisagg)
library(tsibble)
library(argusDS.OxfordEconomics)

########################### DATA LOAD #####################################################################################################

price_vector <- "Price_MiddleEast" #c("Price_US", "Price_China", "Price_Baltic", "Price_MiddleEast", "Price_NorthAfrica")
product_list <- c("Ammonia", "Urea", "Sulphur")
product <- "Sulphur" #c("Ammonia", "Urea", "Sulphur", "DAP")
max_historic_consult = "2023-08-01" #"2023-06-01"   #"2023-02-01"
min_historic_consult  = "2000-01-01"
scalar <- TRUE
percentage_var_and_drivers <- FALSE
#scenario <- "High" #c("Default", "low", "High")
target_month <- month(max_historic_consult)
target_year <- year(max_historic_consult)

#Creates Oxford Economics UOD at monthly frequency
#UOD.final <- UOD_from_OxfordEconomics()
UOD.Oxford <- get_Oxford_UOD(filter = "Main", UOD.final)

#Creates a data frame with Argus Prices imported from the medb
UOD.Argus <- get_Argus_prices(max_historic_consult)

#Incorporates exchange rate data from the New York Federal Reserve
#UOD.Argus <- incorporate_exchange_rate_data(file_location = "NY Fed Currency Data.xlsx", UOD.Argus)

#Takes the monthly or weekly average of the UOD.Argus data frame
UOD.Argus <- convert_frequency(frequency = "monthly", UOD.Argus, max_historic_consult)

#Creates a data frame of GTT trade data
UOD.Trade <- get_gtt_data(product_list = product_list)

#Adds driver forecasts to the UOD.
UOD <- dplyr::full_join(UOD.Argus, UOD.Trade, by = "Date")
UOD <- add_driver_forecasts(UOD, forecast_file = "Forecast Sheet.xlsx", max_historic_consult)
UOD <- dplyr::left_join(UOD, UOD.Oxford, by = "Date")

#Calculates statistics used in high and low scenarios for the create_predict_feed function
gas.sd <- sd(na.omit(UOD$gas.nea))
crude.sd <- sd(na.omit(UOD$crude.icebrent))
gas.mu <- mean(na.omit(UOD$gas.nea))
crude.mu <- mean(na.omit(UOD$crude.icebrent))

#Identifies the price data frame used in the rest of the scripts
Price_df <- get_TARGET_price(product = product, UOD.Argus)
assign(price_vector, Price_df)

#Finalizes UOD, adds additional data from external sources, creates seasonality parameters, performs scalar transformation if specified
other_files_to_add <- c("Covid Data.xlsx")

UOD <- final_UOD_edits(other_files_to_add = "Covid Data.xlsx", scalar = scalar, percentage_var_and_drivers = F, product = product, UOD, Ammonia_forecast, Sulphur_forecast)

#Creates garch residuals (still a work in progress)
garch.df <- create_garch_residuals(i_value = 2, j_value = 2, max_historic_consult, price_vector)

#Creates Price_data data frame
Price_data <- create_Price_data(price_vector = price_vector, UOD)

#Tasks to do: Add currency data, create weekly frequency data frame???





