
library(urca)
library(vars)
library(tsDyn)
#Preparation for VECM trials


commodity = 'Urea'
price_vector <- "Price_MiddleEast"#c("Price_US", "Price_China", "Price_Baltic", "Price_MiddleEast")
scenario = "Default" #c("Default, "Low", "High")
predict_window = 24
scaled_data = FALSE

#Creates a data frame that contains 80 variations of VECM models
VECM_proto <- function(scalar = FALSE, predict_window = 24, scenario = "Default", price_vector = "Price_MiddleEast", max_historic_consult = "2023-08-01", specification = "transitory"){

df <- Price_data %>%
  dplyr::select(TARGET, gas.nea, crude.icebrent)

TARGET.mu <- mean(df$TARGET)
TARET.sd <- sd(df$TARGET)

if(scalar == TRUE){
df <- df %>%
  dplyr::mutate(TARGET = scale(TARGET))
}

predict_feed <- create_predict_feed(price_vector, max_historic_consult = max_historic_consult, predict_window = 24, scenario = scenario, scalar = F)

df2 <- predict_feed %>%
  dplyr::select(gas.nea, crude.icebrent)

df3 <- rbind(df %>% dplyr::select(gas.nea, crude.icebrent), df2)

all_forecasts <- list()

for(i in 2:20)
{
  for(j in 1:2)
    {

ar_aic <- vars::VAR(df, type = "const", lag.max = 19, ic = "AIC", season = 12)
var_aic$p

#can experiment with different cases
#spec defualt is longrun, can also be transitory
#type can be trace or eigen

vec <- urca::ca.jo(df, ecdet = "const", type = "eigen", K = i, spec = specification, season = 12)

summary(vec)

tsDyn::VECM(df, lag = 19, r = 2,
            estim = "ML",
            LRinclude = "none")

coint_ca.jo <- urca::cajorls(vec, r = 2)


vecm_est <- vec2var(vec, r = 2)

vec2var_ca.jo <- vars::vec2var(vec, r = j)

lag_order <- vec2var_ca.jo$p


vec_forecast <- predict(vec2var_ca.jo, n.ahead = nrow(df2), dumvar = df3, dummat = NULL)

TARGET_forecast <- vec_forecast$fcst$TARGET
forecast_name <- paste("TARGET_forecast_lag", i, "_R", j, sep = "")

all_forecasts[[forecast_name]] <- TARGET_forecast

    }

}

return(all_forecasts)

}
all_forecasts3 <- VECM_proto(scalar = FALSE, predict_window = 24, scenario = "Default", price_vector = "Price_MiddleEast", max_historic_consult = "2023-08-01", specification = "transitory")


#Creates error data frame template
create_error_template <- function(model = "VECM", price_vector = pricevector){

  df <- get(price_vector)
  df$ME <- NA
  df$MAE <- NA
  df$MSE <- NA
  df$MRE <- NA
  df$MIS <- NA
  df$MPE <- NA
  df$MAPE <- NA
  df$MASE <- NA
  df$RMSSE <- NA
  df$sMSE <- NA
  df$sPIS <- NA
  df$sCE <- NA
  df <- df %>%
    dplyr::select(-ISO2, -clust, -Region) %>%
    dplyr::filter(Date >= as.Date("2018-01-01"))

  df_name <- paste(model, "Error", sep = ".")
  assign(df_name, df)
  return(get(df_name))

}
VECM.Error <- create_error_template(model = "VECM", price_vector = price_vector)


create_VECM_error_df <- function(scaled_data = scaled_data, specification = "longrun") {

  Forecasts.GasCrude <- read_excel("24 Month Sheet 2.xlsx", sheet = "Sheet1")
  Forecasts.GasCrude$Date <- as.Date(Forecasts.GasCrude$Date)
  MMMYY.start <- unique(sub("\\b(\\w{3})(\\d{2})\\..*", "\\1\\2", names(Forecasts.GasCrude %>% dplyr:: select(-Date))))
  MMMYY.start <- MMMYY.start[-length(MMMYY.start)]

is_first_iteration <- TRUE

for(i in MMMYY.start){

  filtered_cols <- Forecasts.GasCrude %>% dplyr:: select(Date)
  filtered_cols <- cbind(filtered_cols$Date, Forecasts.GasCrude[, grepl(i, colnames(Forecasts.GasCrude))])
  filtered_cols <- na.omit(filtered_cols)
  names(filtered_cols) <- c("Date", "gas.nea", "crude.icebrent")

year <- as.character(as.integer(substr(i, 4, 5)) + 2000)
month.char <- substr(i, 1, 3)  %>% toupper()
month.num <- format(as.Date(paste0(i, "01"), "%b%y%d"), "%m")


UOD.H.name <- paste0("merged.UOD.", year, month.num, month.char, ".DB")
UOD.H <- get(UOD.H.name)

if(as.Date(paste(year, month.num, "01", sep = "-")) < as.Date("2020-07-01")){
  #  if(identical(Forecasts.working, Forecasts18_19)){
  UOD.H <- UOD.H %>%
    dplyr::rename(gas.nea = WPGASJAP_WLD) %>%
    dplyr::select(Date, gas.nea)

  filtered_cols <- filtered_cols %>%
    dplyr::select(Date, crude.icebrent) %>%
    dplyr::left_join(UOD.H, by = "Date")
}


if (scaled_data == TRUE){
  filtered_cols <- filtered_cols %>%
    dplyr::mutate(gas.nea = (gas.nea - gas.mu) / gas.sd) %>%
    dplyr::mutate(crude.icebrent = (crude.icebrent - crude.mu) / crude.sd)
}

present_date = paste(year, month.num, "01", sep = "-")

historical.data <- Price_data %>%
  dplyr::select(Date, gas.nea, crude.icebrent) %>%
  dplyr::filter(Date < as.Date(present_date))
forecast.data <- filtered_cols
historical.and.forecast <- rbind(historical.data, forecast.data) %>% dplyr::select(-Date)

historical.data <- Price_data %>%
  dplyr::filter(Date < as.Date(present_date)) %>%
  dplyr::select(TARGET, gas.nea, crude.icebrent)



if(is_first_iteration == TRUE){
  all_vecm_names <- data.frame()
}

for(lag in 2:6) {

  for(R in 1:2) {

    vec <- urca::ca.jo(historical.data, ecdet = "const", type = "trace", K = lag, spec = specification, season = 12)
    vec2var_ca.jo <- vars::vec2var(vec, r = R)
    vec_forecast <- predict(vec2var_ca.jo, n.ahead = nrow(forecast.data), dumvar = historical.and.forecast, dummat = NULL)

    fcst <- as.data.frame(vec_forecast$fcst$TARGET)
    fcst <- fcst %>%
      dplyr::select(fcst)

    fcst_name <- paste("fcst_lag", lag, "_R", R, sep = "")

    df.fcst <- cbind(filtered_cols, fcst) %>%
      dplyr::select(Date, fcst)
    colnames(df.fcst)[colnames(df.fcst) == "fcst"] <- fcst_name

   ############################################################################################################################################
    #Error Evaluation

    start_date <- as.Date(paste(year, month.num, "01", sep = "-"))
    end_date <- start_date + months(24)

     holdout <- Price_data %>%
      dplyr::select(Date, TARGET) %>%
      dplyr::filter(Date >= as.Date(start_date)) %>%
      dplyr::filter(Date < as.Date(end_date))

    holdout <- holdout$TARGET

    df <- get(price_vector) %>%
      dplyr::select(-ISO2, -clust, -Region) %>%
      dplyr::mutate(TARGET_lag = dplyr::lag(TARGET, n = 1)) %>%
      dplyr::mutate(naive = abs(TARGET - TARGET_lag )) %>%
      na.omit()

    df <- df[, -1]
    scale <- mean(df$naive)

    forecast <- df.fcst
    names(forecast) <- c("Date", "Value")
    forecast <- forecast$Value

    if(as.Date(present_date) <= as.Date("2021-06-01")) {
      x1 <- greybox::ME(holdout = holdout, forecast = forecast)
      x2 <- greybox::MAE(holdout = holdout, forecast = forecast)
      x3 <- greybox::MSE(holdout = holdout, forecast = forecast)
      x4 <- greybox::MRE(holdout = holdout, forecast = forecast)
      #x5 <- greybox::MIS(holdout = holdout, lower = 0.025, upper = 0.975, level = 0.95)
      x6 <- greybox::MPE(holdout = holdout, forecast = forecast)
      x7 <- greybox::MAPE(holdout = holdout, forecast = forecast)
      x8 <- greybox::MASE(holdout = holdout, forecast = forecast, scale = scale)
      x9 <- greybox::RMSSE(holdout = holdout, forecast = forecast, scale = scale)
      x10 <- greybox::sMSE(holdout = holdout, forecast = forecast, scale = scale)
      x11 <- greybox::sPIS(holdout = holdout, forecast = forecast, scale = scale)
      x12 <- greybox::sCE(holdout = holdout, forecast = forecast, scale = scale)

      VECM.Error$ME[which(VECM.Error$Date == as.Date(present_date))] <- x1
      VECM.Error$MAE[which(VECM.Error$Date == as.Date(present_date))] <- x2
      VECM.Error$MSE[which(VECM.Error$Date == as.Date(present_date))] <- x3
      VECM.Error$MRE[which(VECM.Error$Date == as.Date(present_date))] <- x4
      # VECM.Error$MIS[which(VECM.Error$Date == as.Date(i))] <- x5
      VECM.Error$MPE[which(VECM.Error$Date == as.Date(present_date))] <- x6
      VECM.Error$MAPE[which(VECM.Error$Date == as.Date(present_date))] <- x7
      VECM.Error$MASE[which(VECM.Error$Date == as.Date(present_date))] <- x8
      VECM.Error$RMSSE[which(VECM.Error$Date == as.Date(present_date))] <- x9
      VECM.Error$sMSE[which(VECM.Error$Date == as.Date(present_date))] <- x10
      VECM.Error$sPIS[which(VECM.Error$Date == as.Date(present_date))] <- x11
      VECM.Error$sCE[which(VECM.Error$Date == as.Date(present_date))] <- x12

      if(is_first_iteration == TRUE){
        assign(paste("VECM.Error_lag", lag, "_R", R, sep = ""), VECM.Error)
      }

     VECM.Error.temp <- get(paste("VECM.Error_lag", lag, "_R", R, sep = ""))

      VECM.Error.temp[which(VECM.Error.temp$Date == as.Date(paste(year, month.num, "01", sep = "-"))), ] <-
        VECM.Error[which(VECM.Error$Date == as.Date(paste(year, month.num, "01", sep = "-"))), ]

      assign(paste("VECM.Error_lag", lag, "_R", R, sep = ""), VECM.Error.temp)

      # if(is_first_iteration == TRUE){
      #   all_vecm_names <- rbind(all_vecm_names, data.frame(fcst_name))
      #
      #     }
        }
      }
    } # This is the end of the loops to create different price forecasts from the perspective of date `i`

  if(is_first_iteration == TRUE){
    all_vecm_df <- list() }

is_first_iteration <- FALSE

  } # This is the end of the loops to change the starting perspecitve date of `i`


for(lag in 2:6){
  for(R in 1:2){

     df_name <- paste("VECM.Error_lag", lag, "_R", R, sep = "")

     all_vecm_df[[df_name]] <- get(df_name)

  }
}

return(all_vecm_df)
}
all_VECM_df <- create_VECM_error_df(scaled_data = FALSE, specification = "transitory")



#create_hybrid_VECM_error_df <- function(scaled_data = scaled_data, lag = lag, R = R) {

  Forecasts.GasCrude <- read_excel("24 Month Sheet 2.xlsx", sheet = "Sheet1")
  Forecasts.GasCrude$Date <- as.Date(Forecasts.GasCrude$Date)
  MMMYY.start <- unique(sub("\\b(\\w{3})(\\d{2})\\..*", "\\1\\2", names(Forecasts.GasCrude %>% dplyr:: select(-Date))))
  MMMYY.start <- MMMYY.start[-length(MMMYY.start)]

  is_first_iteration <- TRUE

  VECM.Error <- create_error_df(model = "VECM", price_vector = price_vector)

  for(i in MMMYY.start){

    filtered_cols <- Forecasts.GasCrude %>% dplyr:: select(Date)
    filtered_cols <- cbind(filtered_cols$Date, Forecasts.GasCrude[, grepl(i, colnames(Forecasts.GasCrude))])
    filtered_cols <- na.omit(filtered_cols)
    names(filtered_cols) <- c("Date", "gas.nea", "crude.icebrent")

    year <- as.character(as.integer(substr(i, 4, 5)) + 2000)
    month.char <- substr(i, 1, 3)  %>% toupper()
    month.num <- format(as.Date(paste0(i, "01"), "%b%y%d"), "%m")


    UOD.H.name <- paste0("merged.UOD.", year, month.num, month.char, ".DB")
    UOD.H <- get(UOD.H.name)

    if(as.Date(paste(year, month.num, "01", sep = "-")) < as.Date("2020-07-01")){
      #  if(identical(Forecasts.working, Forecasts18_19)){
      UOD.H <- UOD.H %>%
        dplyr::rename(gas.nea = WPGASJAP_WLD) %>%
        dplyr::select(Date, gas.nea)

      filtered_cols <- filtered_cols %>%
        dplyr::select(Date, crude.icebrent) %>%
        dplyr::left_join(UOD.H, by = "Date")
    }


    if (scaled_data == TRUE){
      filtered_cols <- filtered_cols %>%
        dplyr::mutate(gas.nea = (gas.nea - gas.mu) / gas.sd) %>%
        dplyr::mutate(crude.icebrent = (crude.icebrent - crude.mu) / crude.sd)
    }

    present_date = paste(year, month.num, "01", sep = "-")

    historical.data <- Price_data %>%
      dplyr::select(Date, gas.nea, crude.icebrent) %>%
      dplyr::filter(Date < as.Date(present_date))
    forecast.data <- filtered_cols
    historical.and.forecast <- rbind(historical.data, forecast.data) %>% dplyr::select(-Date)

    historical.data <- Price_data %>%
      dplyr::filter(Date < as.Date(present_date)) %>%
      dplyr::select(TARGET, gas.nea, crude.icebrent)


    vec <- urca::ca.jo(historical.data, ecdet = "const", type = "trace", K = lag, spec = "transitory", season = 12)
    vec2var_ca.jo <- vars::vec2var(vec, r = R)
    vec_forecast <- predict(vec2var_ca.jo, n.ahead = nrow(forecast.data), dumvar = historical.and.forecast, dummat = NULL)

    fcst <- as.data.frame(vec_forecast$fcst$TARGET)
    fcst <- fcst %>%
      dplyr::select(fcst)

    fcst_name <- paste("fcst_lag", lag, "_R", R, sep = "")

    df.fcst <- cbind(filtered_cols, fcst) %>%
      dplyr::select(Date, fcst)
    colnames(df.fcst)[colnames(df.fcst) == "fcst"] <- fcst_name

    ############################################################################################################################################
    #Error Evaluation

    start_date <- as.Date(paste(year, month.num, "01", sep = "-"))
    end_date <- start_date + months(24)

    holdout <- Price_data %>%
      dplyr::select(Date, TARGET) %>%
      dplyr::filter(Date >= as.Date(start_date)) %>%
      dplyr::filter(Date < as.Date(end_date))

    holdout <- holdout$TARGET

    df <- get(price_vector) %>%
      dplyr::select(-ISO2, -clust, -Region) %>%
      dplyr::mutate(TARGET_lag = dplyr::lag(TARGET, n = 1)) %>%
      dplyr::mutate(naive = abs(TARGET - TARGET_lag )) %>%
      na.omit()

    df <- df[, -1]
    scale <- mean(df$naive)

    forecast <- df.fcst
    names(forecast) <- c("Date", "Value")
    forecast <- forecast$Value

    if(as.Date(present_date) <= as.Date("2021-06-01")) {
      x1 <- greybox::ME(holdout = holdout, forecast = forecast)
      x2 <- greybox::MAE(holdout = holdout, forecast = forecast)
      x3 <- greybox::MSE(holdout = holdout, forecast = forecast)
      x4 <- greybox::MRE(holdout = holdout, forecast = forecast)
      #x5 <- greybox::MIS(holdout = holdout, lower = 0.025, upper = 0.975, level = 0.95)
      x6 <- greybox::MPE(holdout = holdout, forecast = forecast)
      x7 <- greybox::MAPE(holdout = holdout, forecast = forecast)
      x8 <- greybox::MASE(holdout = holdout, forecast = forecast, scale = scale)
      x9 <- greybox::RMSSE(holdout = holdout, forecast = forecast, scale = scale)
      x10 <- greybox::sMSE(holdout = holdout, forecast = forecast, scale = scale)
      x11 <- greybox::sPIS(holdout = holdout, forecast = forecast, scale = scale)
      x12 <- greybox::sCE(holdout = holdout, forecast = forecast, scale = scale)

      VECM.Error$ME[which(VECM.Error$Date == as.Date(present_date))] <- x1
      VECM.Error$MAE[which(VECM.Error$Date == as.Date(present_date))] <- x2
      VECM.Error$MSE[which(VECM.Error$Date == as.Date(present_date))] <- x3
      VECM.Error$MRE[which(VECM.Error$Date == as.Date(present_date))] <- x4
      # VECM.Error$MIS[which(VECM.Error$Date == as.Date(i))] <- x5
      VECM.Error$MPE[which(VECM.Error$Date == as.Date(present_date))] <- x6
      VECM.Error$MAPE[which(VECM.Error$Date == as.Date(present_date))] <- x7
      VECM.Error$MASE[which(VECM.Error$Date == as.Date(present_date))] <- x8
      VECM.Error$RMSSE[which(VECM.Error$Date == as.Date(present_date))] <- x9
      VECM.Error$sMSE[which(VECM.Error$Date == as.Date(present_date))] <- x10
      VECM.Error$sPIS[which(VECM.Error$Date == as.Date(present_date))] <- x11
      VECM.Error$sCE[which(VECM.Error$Date == as.Date(present_date))] <- x12

      if(is_first_iteration == TRUE){
        assign(paste("VECM.Error_lag", lag, "_R", R, sep = ""), VECM.Error)
      }

      VECM.Error.temp <- get(paste("VECM.Error_lag", lag, "_R", R, sep = ""))

      VECM.Error.temp[which(VECM.Error.temp$Date == as.Date(paste(year, month.num, "01", sep = "-"))), ] <-
        VECM.Error[which(VECM.Error$Date == as.Date(paste(year, month.num, "01", sep = "-"))), ]

      assign(paste("VECM.Error_lag", lag, "_R", R, sep = ""), VECM.Error.temp)

    }

    is_first_iteration <- FALSE
  }

  return(VECM.Error.temp)
#}
#VECM_df <- create_VECM_error_df(scaled_data = FALSE, lag = 2, R = 2)

#assign(paste("VECM.Error_lag", lag, "_R", R, sep = ""), VECM_df)

vecm_name_column <- data.frame()
for(i in 2:7){
  for(j in 1:2){
    vecm_name <- paste("VECM.Error_lag", i, "_R", j, sep = "")
    vecm_name_column <- rbind(vecm_name_column, vecm_name)
  }
}

VECM_avg_Error_df <- data.frame()
# Loop through each data frame in the list
for (i in names(all_VECM_df)) {
  errors <- all_VECM_df[[paste(i)]]
  errors <- errors %>%
    dplyr::select(-MIS) %>%
    na.omit

  df <- data.frame(Location = i, stringsAsFactors = FALSE)
  df$ME <- mean(errors$ME)
  df$MAE <- mean(errors$MAE)
  df$MSE <- mean(errors$MSE)
  df$MRE <- mean(errors$MRE)
  df$MPE <- mean(errors$MPE)
  df$MAPE <- mean(errors$MAPE)
  df$MASE <- mean(errors$MASE)
  df$RMSSE <- mean(errors$RMSSE)
  df$sMSE <- mean(errors$sMSE)
  df$sPIS <- mean(errors$sPIS)
  df$sCE <- mean(errors$sCE)

  VECM_avg_Error_df <- rbind(VECM_avg_Error_df, df)

}

assign("transitory VECM_avg_Error_df", VECM_avg_Error_df)




Errors_df_fullrange <- VECM_avg_Error_df





