UOD_from_OxfordEconomics <- function(){

  #' Imports a variety Oxford Economics macroeconomic forecasts

  #'

  #' Dis-aggregates the quarterly Oxford data to a monthly frequency

  #'


  UOD_basic <- argusDS.OxfordEconomics::get_OxfordEconomics_medb(freq_annual = FALSE)
  UOD_basic <- UOD_basic %>% dplyr::rename(time = publication_datetime)
  UOD_basic$time <- as.Date(UOD_basic$time)
  old_values <- which(UOD_basic$time > as.Date("2026-12-31"))
  UOD_basic <- UOD_basic[!old_values ,]

  #new_data <- UOD_basic %>% dplyr:: group_by(c('ISO2', 'time')) %>% pivot_wider(names_from = IndicatorCode, values_from = value)

  #Renames IndicatorCodes with unusual characters
  UOD_basic$IndicatorCode[UOD_basic$IndicatorCode =='BCU$'] = 'BCUUSD'
  UOD_basic$IndicatorCode[UOD_basic$IndicatorCode =='BLOANS%'] = 'BLOANSPC'
  UOD_basic$IndicatorCode[UOD_basic$IndicatorCode =='CPI%YR'] = 'CPIYR'
  UOD_basic$IndicatorCode[UOD_basic$IndicatorCode =='GC$'] = 'GCUSD'
  UOD_basic$IndicatorCode[UOD_basic$IndicatorCode =='IF!%'] = 'IFPC'
  UOD_basic$IndicatorCode[UOD_basic$IndicatorCode =='GDP%YR'] = 'GDPPCYR'
  UOD_basic$IndicatorCode[UOD_basic$IndicatorCode =='GGDBT%'] = 'GGDBTPC'
  UOD_basic$IndicatorCode[UOD_basic$IndicatorCode =='FDI$IN%'] = 'FDIUSDPC'


  #argusDS.GTT::fct_get_gtt_fertilizer(selectedProduct = "Urea", reporter_ISO2 = "US", direction = "Import")
  #UOD_basic_A <- get_OxfordEconomics_medb(freq_annual = TRUE)



  #Prepares variables for tempdisagg function
  indicatorcodes <- unique(UOD_basic$IndicatorCode)

  drivers_to_remove <- c("RSH", "RLEND", "GDPHEADPPP", "TOT", "DEBT_SEC_INB", "RXCR", "RSH", "RXD_QTR", "RXD", "GDPHEAD", "PPI", "LS", "UP", "C", "M2", "C_QTR", "IF_QTR")

  # drivers_to_remove <- c("RSH", "RLEND", "GDPHEADPPP", "TOT", "CREDR", "DEBT_SEC_INB", "RXCR", "RSH", "RXD_QTR", "RXD", "GDPHEAD", "PPI", "LS", "UP", "RXCR_EPRE", "RLEND", "C", "BCUUSD", "M2", "C_QTR", "GCUSD", "IF_QTR")

  indicatorcodes <- indicatorcodes[!indicatorcodes %in% drivers_to_remove]

  #
  #
  # mean_indc <- c("RXEURO", "RX", "CPI")
  #
  # sum_indc <- c("IP", "DOIL", "DGAS", "FDIUSDPC")

  mean_indc <- c("RXEURO", "RX", "CPIYR", "CPI", "GDPPCYR", "CPI_QTR", "GGDBTPC", "CREDR", "RXCR_EPRE")
  #
  sum_indc <- c("IP", "DOIL", "DGAS", "CUMOD", "BLOANSPC", "IFPC", "IPSA", "FDIUSDPC", "GDP_QTR", "GDPPPP",
                "GVAAGR", "GCUSD", "BCUUSD")

  #indicatorcodes[c(1, 2, 3, 5, 7, 8, 9, 11, 13, 14, 15, 16, 21, 22, 23, 24, 25, 28, 30, 31, 32, 34, 35, 36, 37)]


  #mean_indc_list <- c("RSH", "RLEND", "GDPHEADPPP", "IP", "TOT", "DOIL", "DGAS", "CREDR", "RXD_QTR", "CPIYR", "RXD", "CPI", "GDPHEAD", "GDPPCYR", "CPI_QTR", "PPI", "IPSA", "LS", "GDP_QTR", "UP", "GDPPPP", "RX", "RXEURO", "DEBT_SEC_INB", "RXCR_EPRE")

  #sum_indc <- indicatorcodes[c(4, 6, 10, 12, 17, 18, 19, 20, 26, 27, 29, 33)]
  ISO2_list <- unique(UOD_basic$ISO2)
  #sum_indc_list <- c("BCUUSD", "C", "CUMOD", "BLOANSPC", "IF_QTR", "GCUSD", "IFPC", "C_QTR", "GGDBTPC", "M2", "FDIUSDPC", "GVAAGR")

  monthly.data.subset = data.frame()
  monthly.data <- data.frame()

  dates <- seq(as.Date("2000-01-01"), as.Date("2026-12-01"), by = "3 months")
  monthly.data <- dates %>%
    as.data.frame()
  monthly.data <- dplyr:: rename(monthly.data, time = .)



  #Creates a data frame with each country specific 'mean' driver as a column, quarterly data
  for(x in ISO2_list) {

    df <- UOD_basic %>% dplyr::filter(ISO2 == x)

    for(i in mean_indc) {

      df2 <- df %>% dplyr:: filter(IndicatorCode == i)

      new_col_name <- paste0(i,'_',x)

      monthly.data.subset <- df2[ ,c(4,5)]%>%
        as.data.frame() %>% dplyr::rename(!!new_col_name := value)

      monthly.data <- dplyr:: left_join(monthly.data, monthly.data.subset, by = "time")

    }

    empty_columns <- colSums(is.na(monthly.data)) == nrow(monthly.data)
    monthly.data <- monthly.data[ ,!empty_columns]

  }

  driver_country <- names(monthly.data)[1:length(monthly.data)]


  dates2 <- seq(as.Date("2000-01-01"), as.Date("2026-12-01"), by = "1 months")
  UOD.means <- dates2 %>%
    as.data.frame()
  UOD.means <- dplyr:: rename(UOD.means, time = .)


  #Uses the tempdisagg 'mean' function to convert the quarterly data to monthly
  for(y in 2:length(monthly.data)) {

    monthly.data.subset <- monthly.data[ ,c(1, y)]

    prediction <- td(monthly.data.subset ~ 1, to = "monthly", method = "chow-lin-fixed", conversion = "mean")

    monthly.data.subset <- predict(prediction) %>%
      as.data.frame

    UOD.means <- dplyr:: left_join(UOD.means, monthly.data.subset, by = "time")
  }
  names(UOD.means) <- driver_country
  bad_columns <- colSums(is.na(UOD.means) > 0)
  UOD.means <- UOD.means[ ,!bad_columns]


  #Resets intermediary variables for 'sum' drivers disagreggation
  monthly.data.subset = data.frame()
  monthly.data <- data.frame()

  dates <- seq(as.Date("2000-01-01"), as.Date("2026-12-01"), by = "3 months")
  monthly.data <- dates %>%
    as.data.frame()
  monthly.data <- dplyr:: rename(monthly.data, time = .)


  #Creates a data frame with each country specific 'sum' driver as a column, quarterly data
  for(x in ISO2_list) {

    df <- UOD_basic %>% dplyr::filter(ISO2 == x)

    for(i in sum_indc) {

      df2 <- df %>% dplyr:: filter(IndicatorCode == i)

      new_col_name <- paste0(i,'_',x)

      monthly.data.subset <- df2[ ,c(4,5)]%>%
        as.data.frame() %>% dplyr::rename(!!new_col_name := value)

      monthly.data <- dplyr:: left_join(monthly.data, monthly.data.subset, by = "time")

    }

    empty_columns <- colSums(is.na(monthly.data)) == nrow(monthly.data)
    monthly.data <- monthly.data[ ,!empty_columns]

  }

  driver_country2 <- names(monthly.data)[1:length(monthly.data)]


  dates2 <- seq(as.Date("2000-01-01"), as.Date("2026-12-01"), by = "1 months")
  UOD.sums <- dates2 %>%
    as.data.frame()
  UOD.sums <- dplyr:: rename(UOD.sums, time = .)



  #Uses the tempdisagg 'sum' function to convert the quarterly data to monthly
  for(y in 2:length(monthly.data)) {

    monthly.data.subset <- monthly.data[ ,c(1, y)]

    prediction <- td(monthly.data.subset ~ 1, to = "monthly", method = "chow-lin-fixed", conversion = "sum")

    monthly.data.subset <- predict(prediction) %>%
      as.data.frame

    UOD.sums <- dplyr:: left_join(UOD.sums, monthly.data.subset, by = "time")
  }
  names(UOD.sums) <- driver_country2

  bad_columns2 <- colSums(is.na(UOD.sums) > 0)
  UOD.sums <- UOD.sums[ ,!bad_columns2]


  #Creates a complete data frame with every country specific driver as a column in monthly data form
  UOD.final <- dplyr:: left_join(UOD.means, UOD.sums, by = "time")

  return(UOD.final)
}

get_Argus_prices <- function(max_historic_consult = "2023-07-01"){

  #'  Uses the fct_get_prices_history to pull the price history of different Argus price records from an Oracle data base.
  #'
  #' Re formats data collected from the fct_get_prices_history function
  #'
  #' @param max_historic_consult The maximum date the function pulls data for, this parameter should be identical
  #' across all functions for the same price forecast
  #'

  target_month <- month(max_historic_consult)
  target_year <- year(max_historic_consult)

  df <- fct_get_prices_history(months_back = 240) %>%
    dplyr::distinct() %>%
    dplyr::rename(Date = PUBLICATION_DATE) %>%
    dplyr::mutate(Date = as.Date(Date)) %>%
    dplyr::select(-PERIOD, -CODE_ID, -CODE_DESC, -TIME_STAMP, -CFWD, -PRICE_TYPE, -TIMESTAMP_ID, -QUOTE_PRICETYPE_ID,
                  -FORWARD_TIMING, -FORWARD_YEAR, -FORWARD_PERIOD, -PERIOD) %>%
    tidyr::pivot_wider(names_from = CONJOINED_DESC, values_from = VALUE) %>%
    dplyr::select(-`Ice Brent month, London midday month 1, midpoint`, -`Ice Brent month, London midday month 2, midpoint`,
                  -`UK OTC base load quarter, London midday quarter 1, midpoint`)

  UOD.Argus <- df
  return(UOD.Argus)

}

incorporate_exchange_rate_data <- function(file_location = "NY Fed Currency Data.xlsx", UOD.Argus) {

  #' Experimental function to incorporate exchange rate data to monthly forecasts, at present looking into
  #' using exchange rate data from S3 to minimize the files that need to be maintained
  #'
  #' The data is collected at monthly frequency, in - line with UOD.Argus initially.
  #'
  #'
  #'
  #' @param file_location The file location of the excel sheet containing historical currency data from the
  #' New York Federal Reserve
  #'
  #' @param UOD.Argus The exsting Argus prices data frame to add the currency data
  #' New York Federal Reserve
  #'
  #' @examples

  NY_data <- read_excel("NY Fed Currency Data.xlsx",
                        sheet = "Sheet2", col_types = c("skip",
                                                        "skip", "skip", "skip", "date", "numeric",
                                                        "numeric", "numeric", "numeric",
                                                        "numeric", "numeric", "numeric",
                                                        "numeric", "numeric", "numeric",
                                                        "numeric", "numeric", "numeric",
                                                        "skip", "skip"))



  date_col <- NY_data %>%
    dplyr::mutate(Date = as.Date(Date)) %>%
    dplyr::select(Date)

  NY_data2 <- NY_data %>%
    dplyr::select(-Date)

  NY_data <- replace(NY_data2, NY_data2 == "ND", NA)
  NY_data <- cbind(date_col, NY_data2)

  UOD.Argus <- UOD.Argus %>%
    dplyr::left_join(NY_data, by = "Date")

  return(UOD.Argus)

}

convert_frequency <- function(frequency = "monthly", UOD.Argus, max_historic_consult) {


  #' Aggregates the UOD.Argus data frame from daily frequency to either weekly or monthly frequency.
  #' For the price corresponding to the last month, max_historic_consult, the spot price is used instead.
  #'
  #' @param frequency Determines if the daily UOD.Argus data should be aggregated to weekly or monthly frequency,
  #' at present the function only supports monthly
  #'
  #' @param UOD.Argus The input data frame containing daily frequency price data.
  #'
  #' @param max_historic_consult The cut off date, should be consistent with other functions.
  #'
  #' @examples

  df <- UOD.Argus

  if (frequency == "monthly") {

    df2 <- df %>%
      dplyr::filter(Date < max_historic_consult) %>%
      dplyr::mutate(Date = as.Date(paste0(format(Date, "%Y-%m"), "-01"))) %>%
      group_by(Date) %>%
      summarize(across(where(is.numeric), mean, na.rm = TRUE))

    same_month_year_dates <- df %>%
      dplyr::filter(month(Date) == target_month & year(Date) == target_year)

    last_recorded_prices <- data.frame(as.Date(max_historic_consult))
    names(last_recorded_prices)[1] <- "Date"

    for(col in names(same_month_year_dates %>% dplyr::select(-Date))) {

      .. <- same_month_year_dates %>%
        dplyr::select(all_of(col)) %>%
        na.omit()

      if(sum(..) > 0){
        . <- same_month_year_dates %>%
          dplyr::select(Date, all_of(col)) %>%
          na.omit() %>%
          dplyr::filter(Date == max(Date)) %>%
          dplyr::select(-Date)

        last_recorded_prices[1, col] <- .[[1]]
      }

      if(sum(..) < 1){
        last_recorded_prices[1, col] <- NA
      }
    }

    df2 <- rbind(df2, last_recorded_prices)
  }

  #Work on this later
  # if (frequency == "weekly") {
  #
  #   df2 <- df %>%
  #     dplyr::filter(Date < max_historic_consult) %>%
  #     dplyr::mutate(Date = as.Date(paste0(format(Date, "%Y-%m"), "-01"))) %>%
  #     group_by(Date) %>%
  #     summarize(across(where(is.numeric), mean, na.rm = TRUE))
  #
  #   same_month_year_dates <- df %>%
  #     dplyr::filter(month(Date) == target_month & year(Date) == target_year)
  #
  #   last_recorded_prices <- data.frame(as.Date(max_historic_consult))
  #   names(last_recorded_prices)[1] <- "Date"
  #
  #   for(col in names(same_month_year_dates)[-1]) {
  #
  #     . <- same_month_year_dates %>%
  #       dplyr::select(Date, all_of(col)) %>%
  #       na.omit() %>%
  #       dplyr::filter(Date == max(Date)) %>%
  #       dplyr::select(-Date)
  #     last_recorded_prices[1, col] <- .[[1]]
  #   }
  #
  #   df2 <- rbind(df2, last_recorded_prices)
  # }

  UOD.Argus <- df2
  return(UOD.Argus)
}

get_gtt_data <- function(product_list = c("Ammonia", "Urea", "Sulphur")) {

  #' Creates a monthly frequency data frame containing net global imports and exports for different commodities. The
  #' trade data is collected from GlobalTradeTracker using the argusDS.GTT package function
  #'
  #'
  #'
  #' @param product_list A vector containing each of the products to collect Import and Export data from.
  #'
  #'
  #' @examples



  df <- seq(as.Date("2010-01-01"), by = "month", to = as.Date("2030-01-01")) %>%as.data.frame()
  names(df)[1] <- "Date"

  for (p in product_list){
    trial.gtt <- argusDS.GTT::fct_get_gtt_fertilizer(selectedProduct = p, direction = "Import")

    trade.gtt <- aggregate(Volume ~ Date, trial.gtt, FUN = sum)
    trade.gtt <- trade.gtt %>% dplyr:: rename(Imports = Volume)

    trade.gtt$Imports[which(trade.gtt$Date > as.Date(max_historic_consult))] <- NA
    trade.gtt$Date <- as.Date(trade.gtt$Date)
    names(trade.gtt)[2] <- paste("Imports", p, sep = ".")
    df <- df %>%
      dplyr:: left_join(trade.gtt, by = "Date")

  }

  for (p in product_list){
    trial.gtt <- argusDS.GTT::fct_get_gtt_fertilizer(selectedProduct = p, direction = "Export")

    trade.gtt <- aggregate(Volume ~ Date, trial.gtt, FUN = sum)
    trade.gtt <- trade.gtt %>% dplyr:: rename(Imports = Volume)

    trade.gtt$Imports[which(trade.gtt$Date > as.Date(max_historic_consult))] <- NA
    trade.gtt$Date <- as.Date(trade.gtt$Date)
    names(trade.gtt)[2] <- paste("Exports", p, sep = ".")
    df <- df %>%
      dplyr:: left_join(trade.gtt, by = "Date")

  }

  UOD.Trade <- df
  return(UOD.Trade)
}

get_Oxford_UOD <- function(filter = "Main", UOD.final) {

  #' Using the Oxford data from above, this function filters the data frame to only include countries deemed as relevant.
  #'
  #' This data is used to create a data frame called UOD.Oxford.
  #'
  #' UOD.Oxford is meant to contain the historical data and forecasts originating from Oxford Economics,
  #' in the same frequency and structure as UOD.Argus and UOD.Trade
  #'
  #'
  #'
  #' @param filter A paramter that can take the values 'Main', 'US', 'Baltic', 'China', 'MiddleEast', to determine the criteria to filter UOD.Oxford by.
  #' At present all forecasts use the filter 'Main', which limits country level data to the 10 largest exporters, imports, producers, and consumers of fertilisers.
  #'
  #'  @param UOD.final This function takes the dis aggregated UOD.final data frame as the input, outputing UOD.Oxford
  #'
  #'
  #' @examples


  all_ISO2codes <- c("AF", "AL", "DZ", "AS", "AD", "AO", "AI", "AQ", "AG", "AR", "AM", "AW", "AU", "AT", "AZ", "BS", "BH", "BD", "BB", "BY", "BE", "BZ", "BJ", "BM", "BT", "BO", "BQ", "BA", "BW", "BV", "BR", "IO", "BN", "BG", "BF", "BI", "CV", "KH", "CM", "CA", "KY", "CF", "TD", "CL", "CN", "CX", "CC", "CO", "KM", "CD", "CG", "CK", "CR", "HR", "CU", "CW", "CY", "CZ", "CI", "DK", "DJ", "DM", "DO", "EC", "EG", "SV", "GQ", "ER", "EE", "SZ", "ET", "FK", "FO", "FJ", "FI", "FR", "GF", "PF", "TF", "GA", "GM", "GE", "DE", "GH", "GI", "GR", "GL", "GD", "GP", "GU", "GT", "GG", "GN", "GW", "GY", "HT", "HM", "VA", "HN", "HK", "HU", "IS", "IN", "ID", "IR", "IQ", "IE", "IM", "IL", "IT", "JM", "JP", "JE", "JO", "KZ", "KE", "KI", "KP", "KR", "KW", "KG", "LA", "LV", "LB", "LS", "LR", "LY", "LI", "LT", "LU", "MO", "MG", "MW", "MY", "MV", "ML", "MT", "MH", "MQ", "MR", "MU", "YT", "MX", "FM", "MD", "MC", "MN", "ME", "MS", "MA", "MZ", "MM", "NA", "NR", "NP", "NL", "NC", "NZ", "NI", "NE", "NG", "NU", "NF", "MP", "NO", "OM", "PK", "PW", "PS", "PA", "PG", "PY", "PE", "PH", "PN", "PL", "PT", "PR", "QA", "MK", "RO", "RU", "RW", "RE", "BL", "SH", "KN", "LC", "MF", "PM", "VC", "WS", "SM", "ST", "SA", "SN", "RS", "SC", "SL", "SG", "SX", "SK", "SI", "SB", "SO", "ZA", "GS", "SS", "ES", "LK", "SD", "SR", "SJ", "SE", "CH", "SY", "TW", "TJ", "TZ", "TH", "TL", "TG", "TK", "TO", "TT", "TN", "TR", "TM", "TC", "TV", "UG", "UA", "AE", "GB", "UM", "US", "UY", "UZ", "VU", "VE", "VN", "VG", "VI", "WF", "EH", "YE", "ZM", "ZW", "AX")

  US.filter <- c("France", "Argentina",  "Mexico", "Canada", "Belgium", "Brazil", "Australia", "Uruguay", "United Kingdom", "Chile",
                 "Russian Federation", "Trinidad and Tobago", "Canada", "Poland", "Romania", "Lithuania",  "Ukraine", "Egypt", "Netherlands",
                 "Belarus", "China", "Japan", "South Korea", "Taiwan", "India", "Germany", "Italy", "United States")

  US.filter.small <- c("United States", "India", "Brazil", "Thailand", "Turkey", "Mexico", "Australia", "South Korea", "France", "Italy", "Russia",
                       "China", "Qatar", "Saudi Arabia", "Oman", "Egypt", "Ukraine", "Canada", "United Arab Emirates", "Algeria")

  Baltic.filter <- c("Lithuania", "Belarus",  "Germany", "Poland", "Ukraine", "Latvia", "Turkmenistan", "Austria",
                     "Uzbekistan", "Moldova", "Brazil", "Mexico", "Peru", "Switzerland", "Turkey", "Finland",  "United States", "India",
                     "Thailand", "Australia", "South Korea", "France", "Italy", "Russia", "China", "Qatar", "Saudi Arabia", "Oman", "Egypt",
                     "Ukraine", "Canada", "United Arab Emirates", "Algeria")

  China.filter <- c("India", "South Korea",  "United States", "Vietnam", "Mexico", "Bangladesh", "Philippines", "Chile",
                    "Taiwan", "Japan", "Ukraine", "Uzbekistan", "Iran", "Russia", "Indonesia", "Oman",  "United Arab Emirates", "Malaysia",
                    "Bahrain", "Brazil", "Turkey", "Australia", "France", "Italy", "China", "Qatar", "Saudi Arabia", "Egypt",
                    "Canada", "Algeria")

  MiddleEast.filter <- c("India", "United States",  "Thailand", "Australia", "Brazil", "South Africa", "Turkey", "Bangladesh",
                         "New Zealand", "Singapore", "Ukraine", "Egypt", "Iran", "Oman", "Israel", "Jordan",
                         "Russia", "Romania",  "Turkmenistan", "Libya", "Saudi Arabia", "Croatia", "Mexico", "South Korea",
                         "France", "Italy", "China", "Qatar", "Canada", "United Arab Emirates", "Algeria")

  NorthAfrica.filter <- c("France", "Turkey",  "United States", "Italy", "Spain", "United Kingdom", "Brazil", "India",
                          "Argentina", "Greece", "Russia", "Ukraine", "Libya", "Romania", "Egypt", "Croatia",  "Turkey",
                          "Syria", "Germany", "Thailand", "Mexico", "Australia", "South Korea", "China", "Qatar", "Saudi Arabia",
                          "Oman", "Canada", "United Arab Emirates", "Algeria")

  Main.filter <- c("China", "India", "United States", "Indonesia", "Russia", "Pakistan", "Egypt", "Canada", "Qatar", "Saudi Arabia",
                   "Brazil", "Bangladesh", "Germany", "Iran", "Ukraine", "Oman", "United Arab Emirates", "Thailand", "Turkey", "Mexico",
                   "Australia", "Italy", "France", "Vietnam")

  country_codes_argus = argusDS.FZ.utils::consult_names%>%
    dplyr::select(Country, Region, ISO2)

  US.ISO2filter <- countrycode::countrycode(sourcevar = US.filter, origin = "country.name", destination = "iso2c")
  Baltic.ISO2filter <- countrycode::countrycode(sourcevar = Baltic.filter, origin = "country.name", destination = "iso2c")
  China.ISO2filter <- countrycode::countrycode(sourcevar = China.filter, origin = "country.name", destination = "iso2c")
  NorthAfrica.ISO2filter <- countrycode::countrycode(sourcevar = NorthAfrica.filter, origin = "country.name", destination = "iso2c")
  MiddleEast.ISO2filter <- countrycode::countrycode(sourcevar = MiddleEast.filter, origin = "country.name", destination = "iso2c")
  Main.ISO2filter <- countrycode::countrycode(sourcevar = Main.filter, origin = "country.name", destination = "iso2c")

  cols_to_keep_US <- grep(paste0("_(", paste(US.ISO2filter, collapse = "|"), ")$"), colnames(UOD.final), value = TRUE)
  US.UOD.final <- UOD.final[, c("time", cols_to_keep_US)]

  cols_to_keep_China <- grep(paste0("_(", paste(China.ISO2filter, collapse = "|"), ")$"), colnames(UOD.final), value = TRUE)
  China.UOD.final <- UOD.final[, c("time", cols_to_keep_China)]

  cols_to_keep_Baltic <- grep(paste0("_(", paste(Baltic.ISO2filter, collapse = "|"), ")$"), colnames(UOD.final), value = TRUE)
  Baltic.UOD.final <- UOD.final[, c("time", cols_to_keep_Baltic)]

  cols_to_keep_NorthAfrica <- grep(paste0("_(", paste(NorthAfrica.ISO2filter, collapse = "|"), ")$"), colnames(UOD.final), value = TRUE)
  NorthAfrica.UOD.final <- UOD.final[, c("time", cols_to_keep_NorthAfrica)]

  cols_to_keep_MiddleEast <- grep(paste0("_(", paste(MiddleEast.ISO2filter, collapse = "|"), ")$"), colnames(UOD.final), value = TRUE)
  MiddleEast.UOD.final <- UOD.final[, c("time", cols_to_keep_MiddleEast)]

  cols_to_keep_Main <- grep(paste0("_(", paste(Main.ISO2filter, collapse = "|"), ")$"), colnames(UOD.final), value = TRUE)
  Main.UOD.final <- UOD.final[, c("time", cols_to_keep_Main)]

  if(filter=="Main") {
    UOD.Oxford = Main.UOD.final %>% dplyr::rename(Date=time)}

  if(filter=="US") {
    UOD.Oxford = US.UOD.final %>% dplyr::rename(Date=time)}

  if(filter=="China") {
    UOD.Oxford = China.UOD.final %>% dplyr::rename(Date=time)}

  if(filter=="Baltic") {
    UOD.Oxford = Baltic.UOD.final %>% dplyr::rename(Date=time)}

  if(filter=="NorthAfrica") {
    UOD.Oxford = NorthAfrica.UOD.final %>% dplyr::rename(Date=time)}

  if(filter=="MiddleEast") {
    UOD.Oxford = MiddleEast.UOD.final %>% dplyr::rename(Date=time)}

  return(UOD.Oxford)
}

add_driver_forecasts <- function(UOD, forecast_file = "Forecast Sheet.xlsx", max_historic_consult = "2023-07-01"){

  #' Incorporates the forecasts of price and trade data to include in our forecasts. The historical records at this point are contained in UOD.
  #'
  #' @param UOD The data frame that will now contain both historical and forecast data
  #'
  #' @param forecast_file The excel sheet that holds the relevant forecast data, has to be maintained manually
  #'
  #' @param max_historic_consult The cut off date, should be the month for where we have our most recent forecasts
  #'
  #' @examples

  Forecast <- read_excel("Forecast Sheet.xlsx", sheet = paste(max_historic_consult)) %>%
    dplyr::rename(Date=Time) %>%
    dplyr::mutate(Date = as.Date(Date)) %>%
    dplyr::mutate(Imports.Ammonia = Imports.Ammonia * 1000) %>%
    dplyr::mutate(Imports.Sulphur = Imports.Sulphur * 1000) %>%
    dplyr::mutate(Imports.Urea = Imports.Urea * 1000) %>%
    dplyr::select(-crude.nymexwti, -crude.uralsnwe, -crude.uralsmed, -crude.wtimidland, -crude.mars, -crude.dubai, -crude.murban )

  # colnames.F <- c("Date", "gas.ttf", "gas.nea", "coal.ara6", "coal.rb6", "coal.newcastle6", "coal.newcastle5.5", "crude.icebrent", "crude.northseadated",
  #                 "crude.cpcblend", "crude.quaiboe", "crude.dalia", "crude.wtihouston", "crude.wcshardisty", "crude.wcshouston", "crude.espoblend",
  #                 "Imports.Sulphur", "Imports.Urea", "Imports.Ammonia", "Wheat")
  #
  # UOD.equivalent <- c("Date", "Natural gas TTF $/mnBtu month, London close month 0, midpoint",
  #                     "LNG des Northeast Asia (ANEA) half-month, No time stamp half-month 1, midpoint", "Coal ARA 6000kcal NAR cif, London close, midpoint",
  #                     "Coal Richards Bay 6000kcal NAR fob, London close, midpoint",
  #                     "Coal Newcastle 6000kcal NAR fob, London close, midpoint", "Coal Newcastle 5500kcal NAR fob, London close, midpoint",
  #                     "Ice Brent month, Singapore close month 1, midpoint", "North Sea Dated, London close, midpoint", "CPC Blend fob, London close, midpoint",
  #                     "Qua Iboe, London close, midpoint", "Dalia, London close, midpoint", "WTI fob USGC, Houston close, midpoint",
  #                     "WCS Hardisty month, Houston close month 1, midpoint", "WCS Houston month, Houston close month 1, midpoint",
  #                     "ESPO blend fob Kozmino, Singapore close, midpoint", "Imports.Sulphur", "Imports.Urea", "Imports.Ammonia",
  #                     "NYSE Euronext Liffe Paris Milling Wheat Futures month, Exchange settlement month 1, settlement")


  load("Name conversion table.rda")
  names.conversion <- names.conversion[names.conversion$colnames.F %in% names(Forecast) ,]

  values_with_forecasts <- names(UOD)[names(UOD) %in% names.conversion$UOD.equivalent]

  UOD <- UOD %>%
    dplyr::select(all_of(values_with_forecasts))

  for(i in 1:length(names.conversion$colnames.F)){

    a <- names.conversion[i, 1]
    b <- names.conversion[i, 2]
    colnames(UOD)[colnames(UOD) == b] <- a
  }

  good.names <- colnames(UOD %>% dplyr::select(-Date))
  UOD <- dplyr::left_join(UOD, Forecast, by = "Date")

  for(i in good.names) {
    name.x <- paste0(i, ".x")
    name.y <- paste0(i, ".y")

    temp = UOD %>% dplyr::select(Date, name.x, name.y)
    colnames(temp) = c("Date", "temporary.x", "temporary.y")

    UOD <- UOD %>%
      dplyr::left_join(temp, by = "Date") %>%
      dplyr::mutate(new_column = coalesce(temporary.y, temporary.x)) %>%
      dplyr::select(-name.x, -name.y, -temporary.x, -temporary.y)

    colnames(UOD)[colnames(UOD) == "new_column"] <- i
  }

  return(UOD)
}

get_TARGET_price <- function(product = "Urea", UOD.Argus){

  #' Identifies the target price to forecast. The Price_df will return the historical price records of the target price. This price will now be called TARGET.
  #'
  #' @param product The function identifies the price to forecast from the product. This is usually the MiddleEast fob, but can vary for specific products
  #'
  #' @param UOD.Argus As Price_df only included the historical price records of TARGET, this is taken from UOD.Argus as the information should be present there.
  #'
  #' @examples


  if(product == "Urea"){
    Price_df = UOD.Argus %>%
      dplyr::select(Date, `Urea granular bulk fob Middle East - non US netback, No time stamp, midpoint`) %>%
      dplyr::rename(TARGET=`Urea granular bulk fob Middle East - non US netback, No time stamp, midpoint`) %>%
      dplyr::filter(Date>=as.Date("2010-01-01")) %>% dplyr::mutate(ISO2=as.factor('SA'), clust=as.factor('Saudi Arabia'), Region=as.factor("Middle East"))
  }

  if(product == "Ammonia"){
    Price_df = UOD.Argus %>%
      dplyr::select(Date, `Ammonia fob Middle East, No time stamp, midpoint`) %>%
      dplyr::rename(TARGET=`Ammonia fob Middle East, No time stamp, midpoint`) %>%
      dplyr::filter(Date>=as.Date("2010-01-01")) %>%
      dplyr::mutate(ISO2=as.factor('SA'), clust=as.factor('Saudi Arabia'), Region=as.factor("Middle East"))
  }

  if(product == "Sulphur"){
    Price_df = UOD.Argus %>%
      dplyr::select(Date, `Sulphur dry bulk fob Middle East, No time stamp, midpoint`) %>%
      dplyr::rename(TARGET=`Sulphur dry bulk fob Middle East, No time stamp, midpoint`) %>%
      dplyr::filter(Date>=as.Date("2010-01-01")) %>%
      dplyr::mutate(ISO2=as.factor('SA'), clust=as.factor('Saudi Arabia'), Region=as.factor("Middle East"))
  }

  if(product == "DAP"){
    Price_df = UOD.Argus %>%
      dplyr::select(Date, `DAP bulk fob Morocco, No time stamp, midpoint`) %>%
      dplyr::rename(TARGET=`DAP bulk fob Morocco, No time stamp, midpoint`) %>%
      dplyr::filter(Date>=as.Date("2010-01-01")) %>%
      dplyr::mutate(ISO2=as.factor('MA'), clust=as.factor('Morocco'), Region=as.factor("North Africa"))
  }

  # if(product == "DAP"){
  #   Price_MiddleEast = Prices %>% dplyr::select(Date, `DAP Middle East`) %>% dplyr::rename(TARGET=`DAP Middle East`) %>% dplyr::filter(Date>=as.Date("2010-01-01")) %>% dplyr::mutate(ISO2=as.factor('SA'), clust=as.factor('Saudi Arabia'), Region=as.factor("Middle East"))
  # }
  return(Price_df)
}

final_UOD_edits <- function(other_files_to_add = "Covid Data.xlsx", scalar = T, percentage_var_and_drivers = F, product = "Urea", UOD, Ammonia_forecast = NA, Sulphur_forecast = NA) {

  #' Includes other external data sources that could be relevant, such as global Covid data that could be used to explain price shocks during the pandemic.
  #' Also performs relevant transformations to the data, and allows for certain products to include the forecasts of other fertiliser products
  #'
  #' @param other_files_to_add Contains a link to the excel sheet containing Covid data, this data is collected from the United Nations World Health Organisation.
  #' As it relates to historical data during the pandemic, values on new infections, deaths, are set to zero in the post-pandemic period.
  #'
  #' @param scalar A true or false parameter that determines if the data should be scaled. At present this involves using the 'scale()' function, which subtracts
  #' the sample mean and divides by the standard error of each data series in UOD
  #'
  #' @param UOD The data frame that contains historical and forecast records for every driver. All operations work to modify this data frame.
  #'
  #' @param percentage_var_and_drivers A true or false parameter that determines if the data should calculate the percentage change from one month to the next.
  #' At present this operation does not function
  #'
  #' @param product If product is set to "DAP", it allows for UOD to include forecasts of related fertiliser commodities stored in Ammonia_forecast, Sulphur_forecast.
  #' In the future the final_UOD_edits function will contain the specific ordering for which fertiliser forecasts can depend on which other fertiliser commodities
  #'
  #' @param Ammonia_forecast A data frame containing ammonia historical prices and forecasts for 24 months into the future
  #'
  #' @param Sulphur_forecast A data frame containing sulphur historical prices and forecasts for 24 months into the future
  #'
  #' @examples



  Covid_Data <- read_excel(paste(other_files_to_add),
                           col_types = c("text", "numeric", "numeric",
                                         "numeric", "numeric", "numeric",
                                         "numeric", "numeric", "numeric",
                                         "numeric", "numeric", "numeric",
                                         "numeric"))

  Covid_Data$Date <- as.Date(Covid_Data$Date)
  Covid_Data_monthly <- aggregate(Covid_Data[,-1], by = list(format(Covid_Data$Date, "%Y-%m")), FUN = sum)
  Covid_Data_monthly <- Covid_Data_monthly %>% dplyr:: rename('Date' = Group.1)
  Covid_Data_monthly$Date <- paste(Covid_Data_monthly$Date, "01", sep = "-")
  Covid_Data_monthly$Date <- as.Date(Covid_Data_monthly$Date)
  Covid_Data_monthly <- Covid_Data_monthly %>% dplyr:: filter(Date <= as.Date("2022-11-01"))

  UOD <- UOD %>%
    dplyr::left_join(Covid_Data_monthly, by = "Date") %>%
    dplyr::mutate_at(vars(total_cases, new_cases, new_cases_smoothed, total_deaths, new_deaths, new_deaths_smoothed, total_cases_per_million,
                          new_cases_per_million, new_cases_smoothed_per_million, total_deaths_per_million, new_deaths_per_million,
                          new_deaths_smoothed_per_million), ~replace_na(., 0))

  if(product == "DAP"){
    UOD <- UOD %>%
      dplyr::left_join(Ammonia_forecast, by = "Date") %>%
      dplyr::left_join(Sulphur_forecast, by = "Date")
  }

  if (scalar == TRUE){
    standardized_UOD <- scale(UOD %>% dplyr::select(-Date))
    standardized_UOD <- as.data.frame(standardized_UOD)
    standardized_UOD <- cbind(UOD %>% dplyr::select(Date), standardized_UOD)
    UOD <- standardized_UOD
  }

  if (percentage_var_and_drivers == TRUE){
    UOD_percentage <- UOD
    UOD_percentage_names <- colnames (UOD %>% dplyr::select(-Date))

    for(col in colnames(UOD_percentage_names)) {
      UOD_percentage[[col]] <- c(NA, diff(UOD[[col]]) / lag(UOD[[col]], default = UOD[[col]][1]))
      UOD <- UOD_percentage
    }


  }


  UOD$month <- factor(format(UOD$Date, "%b"), levels = month.abb)
  UOD$intervention_2021 <- as.numeric(UOD$Date >= "2021-09-01" & UOD$Date <= "2021-11-01")
  UOD$intervention_2022 <- as.numeric(UOD$Date >= "2022-03-01" & UOD$Date <= "2022-03-01")
  UOD$intervention_pandemic <- as.numeric(UOD$Date >= "2021-01-01" & UOD$Date <= "2022-12-01")
  UOD$time <- seq(1, length(UOD$Date), 1)

  UOD <- UOD %>%
    dplyr::mutate(quarter = ceiling(month(Date)/3)) %>%
    dplyr::relocate(quarter, .after = month) %>%
    dplyr::mutate(quarter = as.factor(quarter)) %>%
    dplyr::relocate(time, .after = Date)

  UOD$quarter <- as.factor(UOD$quarter)
  UOD$quarter <- UOD$quarter %>% droplevels()
  UOD$month <- UOD$month %>% droplevels()

  return(UOD)
}

create_garch_residuals <- function(i_value = 2, j_value = 2, max_historic_consult, price_vector) {

  #' Creates garch residual values that could help explain heteroskedasticity in the target price, on the assumption that the variance of the target is
  #' not constant and has time series autocorrelation. T
  #'
  #' This function creates a data frame called garch_df where the columns contain the garch residuals created by a garch model with different AR and MA components.
  #' These residuals are then lagged by one value to ensure that for forecasts, a lagged garch residual value can then be calculated for the next forecasted value.
  #'
  #' @param price_vector Creating the garch residuals requires the data frame corresponding to price_vector
  #'
  #' @param max_historic_consult The max_historic_consult parameter should be consistent across functions to have the functions be compatible
  #'
  #' @param i_value Determines the maximum order of magnitude of the garch AR component
  #'
  #' @param j_value Determines the maximum order of magnitude of the garch MA component
  #'
  #' @examples


  date.temp <- seq(from = as.Date("2010-01-01"), to = as.Date(max_historic_consult) + months(1), by = "months") %>% as.data.frame()
  names(date.temp) <- "Date"

  df <- get(price_vector)

  price.temp <- date.temp %>%
    dplyr::left_join(df, by = "Date") %>%
    dplyr::select(Date, TARGET) %>%
    dplyr::mutate(TARGET_l1 = dplyr::lag(TARGET)) %>%
    dplyr::mutate(log_TARGET_l1 =  argusDS.FZ.utils::log0(TARGET_l1))

  price.temp <- na.omit(price.temp)

  garch.df <- date.temp %>%
    dplyr::filter(Date >= as.Date("2010-03-01"))


  #Creates data frame garch.df that holds the residuals of different garch processes
  for(a in 1:2) {

    if(a==1){
      garch.input <- ts(price.temp$TARGET_l1, start = as.Date("2010-02-01"))
    }

    if(a==2){
      garch.input <- ts(price.temp$log_TARGET_l1, start = as.Date("2010-02-01"))
    }


    for(j in 1:i_value){
      for(k in 0:j_value){

        if(a==1){
          name.temp <- paste("TARGET_l1.r.garch", j, k, sep = ".")
        }

        if(a==2){
          name.temp <- paste("log_TARGET_l1.r.garch", j, k, sep = ".")
        }

        garch_spec <- rugarch::ugarchspec(variance.model = list(model = "sGARCH", garchOrder = c(j, k)),
                                          mean.model = list(armaOrder = c(1, 0)),
                                          distribution.model = "std")

        rugarch.temp <- rugarch::ugarchfit(spec = garch_spec, data = garch.input)
        garch_residuals <- rugarch.temp@fit$residuals
        new_df <- data.frame(garch_residuals)
        colnames(new_df) <- name.temp

        garch.df <- cbind(garch.df, new_df)
      }
    }
  }
  return(garch.df)
}

create_Price_data <- function(price_vector = "Price_MiddleEast", UOD) {

  #' Creates a data frame called Price_data that is limited to the historical past, only including time series values and dates for which there is full data.
  #'
  #' Price_data is not allowed to have null values, as it is the data frame fed into the gamlss step selection process.
  #'
  #' The Price_data data frame must ensure that any parameters that are factors are labelled as such, and have drop_levels() applied
  #'
  #' @param price_vector The data frame name that corresponds to the data frame containing the target price
  #'
  #' @param UOD The data frame that contains anll historical and forecast data. In this context it is only used to extract forecast data.
  #' @examples

  Price_data = get(price_vector) %>% #rbind(Price_US, Price_China, Price_Baltic, Price_MiddleEast) %>% #dplyr::mutate(ISO2=as.factor('US'), clust=as.factor('USA'), Region=as.factor("North America")) %>%
    dplyr::mutate(TARGET_l1 = dplyr::lag(TARGET)) %>%
    dplyr::mutate(TARGET_l2 = dplyr::lag(TARGET_l1)) %>%
    dplyr::mutate(TARGET_l3 = dplyr::lag(TARGET_l2)) %>%
    dplyr::mutate(TARGET_l4 = dplyr::lag(TARGET_l3)) %>%
    dplyr::mutate(TARGET_l5 = dplyr::lag(TARGET_l4)) %>%
    dplyr::mutate(log_TARGET = argusDS.FZ.utils::log0(TARGET)) %>%
    dplyr::mutate(log_TARGET_l1 = argusDS.FZ.utils::log0(TARGET_l1)) %>%
    dplyr::mutate(log_TARGET_l2 = argusDS.FZ.utils::log0(TARGET_l2)) %>%
    dplyr::mutate(log_TARGET_l3 = argusDS.FZ.utils::log0(TARGET_l3)) %>%
    dplyr::mutate(log_TARGET_l4 = argusDS.FZ.utils::log0(TARGET_l4)) %>%
    dplyr::mutate(log_TARGET_l5 = argusDS.FZ.utils::log0(TARGET_l5)) %>%
    dplyr::mutate(log_TARGET.r.ma1 = residuals(Arima(log_TARGET, order = c(0, 0, 1)))) %>%
    dplyr::mutate(log_TARGET.r.ma2 = residuals(Arima(log_TARGET, order = c(0, 0, 2)))) %>%
    dplyr::mutate(log_TARGET.r.ma3 = residuals(Arima(log_TARGET, order = c(0, 0, 3)))) %>%
    dplyr::mutate(log_TARGET.r.ma4 = residuals(Arima(log_TARGET, order = c(0, 0, 4)))) %>%
    dplyr::mutate(log_TARGET.r.ma5 = residuals(Arima(log_TARGET, order = c(0, 0, 5)))) %>%
    dplyr::mutate(log_TARGET_l1.r.ma1 = residuals(Arima(log_TARGET_l1, order = c(0, 0, 1)))) %>%
    dplyr::mutate(log_TARGET_l1.r.ma2 = residuals(Arima(log_TARGET_l1, order = c(0, 0, 2)))) %>%
    dplyr::mutate(log_TARGET_l1.r.ma3 = residuals(Arima(log_TARGET_l1, order = c(0, 0, 3)))) %>%
    dplyr::mutate(log_TARGET_l1.r.ma4 = residuals(Arima(log_TARGET_l1, order = c(0, 0, 4)))) %>%
    dplyr::mutate(log_TARGET_l1.r.ma5 = residuals(Arima(log_TARGET_l1, order = c(0, 0, 5)))) %>%
    dplyr::mutate(TARGET_l1.r.ma1 = residuals(Arima(TARGET_l1, order = c(0, 0, 1)))) %>%
    dplyr::mutate(TARGET_l1.r.ma2 = residuals(Arima(TARGET_l1, order = c(0, 0, 2)))) %>%
    dplyr::mutate(TARGET_l1.r.ma3 = residuals(Arima(TARGET_l1, order = c(0, 0, 3)))) %>%
    dplyr::mutate(TARGET_l1.r.ma4 = residuals(Arima(TARGET_l1, order = c(0, 0, 4)))) %>%
    dplyr::mutate(TARGET_l1.r.ma5 = residuals(Arima(TARGET_l1, order = c(0, 0, 5)))) %>%
    dplyr::left_join(garch.df, by = "Date")

  Price_data <- Price_data %>% dplyr::filter(Date>as.Date("2014-07-01") & Date<=as.Date(max_historic_consult)) %>%   # additional script to get actual lag value on Date 2013-08-01, rather than cutting off one more row to start from 2013-09-01
    dplyr::left_join(UOD %>% dplyr::filter(Date>=as.Date("2014-07-01") & Date <= as.Date(max_historic_consult)), by = 'Date')

  ISO2 <- c('US', 'CN', 'RU', 'SA', 'MA')
  Pvector <- c("Price_US", "Price_China", "Price_Baltic", "Price_MiddleEast", "Price_NorthAfrica")
  identifier <- data.frame(Pvector, ISO2) %>%
    dplyr:: filter(Pvector == price_vector)

  Price_data <- Price_data %>%
    dplyr::select_if(~ !any(is.na(.)))
  #Price_data <- na.omit(Price_data)

  Price_data <- Price_data %>% dplyr::filter(ISO2 == unique(identifier$ISO2))
  Price_data$clust <- Price_data$clust %>% droplevels()
  Price_data$month = Price_data$month %>% droplevels()
  Price_data$Region = Price_data$Region %>% droplevels()
  Price_data$ISO2 = Price_data$ISO2 %>% droplevels()
  Price_data = Price_data %>% dplyr::ungroup()
  Price_data$ISO2_clust = Price_data$ISO2 %>% droplevels()



  return(Price_data)
}

